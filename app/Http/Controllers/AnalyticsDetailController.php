<?php

namespace App\Http\Controllers;

use App\Models\AnalyticsDetail;
use Illuminate\Http\Request;

class AnalyticsDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AnalyticsDetail  $analyticsDetail
     * @return \Illuminate\Http\Response
     */
    public function show(AnalyticsDetail $analyticsDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AnalyticsDetail  $analyticsDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(AnalyticsDetail $analyticsDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AnalyticsDetail  $analyticsDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AnalyticsDetail $analyticsDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AnalyticsDetail  $analyticsDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(AnalyticsDetail $analyticsDetail)
    {
        //
    }
}
