<?php

namespace App\Http\Controllers;

use App\Models\WebsiteDetail;
use Illuminate\Http\Request;

class WebsiteDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebsiteDetail  $websiteDetail
     * @return \Illuminate\Http\Response
     */
    public function show(WebsiteDetail $websiteDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebsiteDetail  $websiteDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(WebsiteDetail $websiteDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebsiteDetail  $websiteDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebsiteDetail $websiteDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WebsiteDetail  $websiteDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebsiteDetail $websiteDetail)
    {
        //
    }
}
